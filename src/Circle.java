
public class Circle {
	// properties
	private double radius;

	// constructor
	Circle(double radius) {
		this.radius = radius;
	}

	// custom method
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	// functions
	public static void area(double radius) {
		double area = 3.14 * radius * radius;
		System.out.println("Area of the circle :" + area);

	}

	public static void diameter(double radius) {
		double diameter = 2 * radius;
		System.out.println("Area of the diameter :" + diameter);
	}

	public static void circumference(double radius) {
		double circumference = 2 * 3.14 * radius;
		System.out.println("Area of the diameter :" + circumference);
	}

}
